"""Reading bai files, and storing the results in the database"""

from tkinter import filedialog
from tkinter import *
from tkinter import messagebox

import csv
import datetime
from decimal import Decimal, DecimalException


class BaiFileException(BaseException):
    pass

class RecordcountMismatchException(BaiFileException):
    pass

class AmountMismatchException(BaiFileException):
    pass

class BadElementCountException(BaiFileException):
    pass

class InvalidFileStructureException(BaiFileException):
    pass



def read_bai_file_fileselect():
    """read bai file """
    print('read_bai_file')
    filename =  filedialog.askopenfilename(initialdir = "/",title = "Select file",filetypes = (("bai files","*.txt"),("all files","*.*")))
    print (filename)
    try:
        read_one_bai_file (filename)
    except BaiFileException as e:
        print('ERROR in reading the file',e)
        messagebox.showerror("ERROR",e)
    except:
        messagebox.showerror("ERROR","unhandled exception")

def DateTimeConverter(date_string, time_string = None):
    """convert a date in YYMMDD time HHMI into an actual date object"""
    if not isinstance(date_string, str):
        raise Exception('DateConverter needs a string as input')
    year = int(date_string[0:2])
    month = int(date_string[2:4])
    day = int(date_string[4:6])

    hours = -1
    minutes = -1

    if time_string:
        if not isinstance(time_string, str):
            raise Exception('DateConverter needs time string as input')
        if time_string == '9999':
            hours = 23
            minutes = 59
        else:
            hours = int(time_string[0:2])
            minutes = int(time_string[2:4])

    if hours == -1:
        rtn = datetime.datetime(year+2000, month, day)
        return rtn
    else:
        rtn = datetime.datetime(year+2000, month, day, hours, minutes)
        return rtn
    return None



class FileHeader:
    """bai file header/trailer"""
    def __init__(self, row, rows_used):
        if (len(row) > 9):
            print("Too many elements in FileHeader ", row)
            raise BadElementCountException('Fileheader in bai file has too many elements: expecting 9 got ' + str(len(row)))
        if (len(row) < 9):
            print("Not enough elements in FileHeader ", row)
            raise BadElementCountException('Fileheader in bai file has too few elements: expecting 9 got ' + str(len(row)))

        self.rows_used_header = rows_used
        self.group_list = []
        self.sender_id = row[1]
        self.receiver_id = row[2]
        self.file_creation_dt = row[3]
        #add check that date is in a good format YYMMDD
        self.file_creation_time = row[4]
        #add check that time is in a good format HHMM  24 hour time
        DateTimeConverter(self.file_creation_dt)
        self.date_time = DateTimeConverter(self.file_creation_dt, self.file_creation_time)

        self.file_id_number = row[5]
        self.physical_record_length = row[6]
        self.block_size = row[7]
        self.version_number = row[8]
        if not self.version_number.endswith('/'):
            print("FileHeader expects a / at end of line")
        else:
            self.version_number = self.version_number[:-1]
        print('file header version '+self.version_number)

    def __repr__(self):
        return 'file '+self.file_id_number + ' ' + self.file_creation_dt + ' ' + self.file_creation_time

    def add_group(self,new_group):
        """add a group to the file"""
        self.group_list.append(new_group)

    def file_trailer(self, row, rows_used):
        """file trailer"""
        self.rows_used_trailer = rows_used
        self.control_total = int(row[1])
        if self.control_total == self.get_group_totals():
            print('   GOOD NEWS: the group totals matches expectations '+str(self.control_total))
        else:
            print('ERROR: group control total '+str(self.control_total) + ' does not match total calculated '+str(self.get_group_totals()))
            raise AmountMismatchException('File trailer total mismatch expecting '+str(self.control_total)+ ' but got '+str(self.get_group_totals()) )

        self.num_of_groups = int(row[2])
        if self.num_of_groups == self.get_group_count():
            print('   GOOD NEWS: the number of groups matches expectations %d '%self.num_of_groups )
        else:
            print('ERROR: number of expected groups %d does not match number of groups found %d'%(self.num_of_groups,self.get_group_count() ))
            raise BadElementCountException('number of expected groups %d does not match number of groups found %d'%(self.num_of_groups,self.get_group_count() ))

        if not row[3].endswith('/'):
            print("file trailer expects a / at end of line")
        else:
            self.num_of_records = int(row[3][:-1])

        if self.num_of_records == self.get_record_count():
            print('   GOOD NEWS: the number of records in the file matches expectations %d '%self.num_of_records)
        else:
            print('ERROR: the number of records expected the file %d  does not match the number found %d'%(self.num_of_records,self.get_record_count()))
            raise RecordcountMismatchException('The number of records expected in the file %d  does not match the number found %d'%(self.num_of_records,self.get_record_count()))

    def get_group_count(self):
        """file header get group count"""
        return len(self.group_list)

    def get_record_count(self):
        """file header get record count"""
        total_records = 0
        for g in self.group_list:
            total_records += g.get_record_count()
        return total_records + self.rows_used_header + self.rows_used_trailer

    def get_group_totals(self):
        """file header get group totals"""
        total = 0.0
        for g in self.group_list:
            total += g.get_group_totals()
        return total

file_header = None

class GroupHeader:
    def __init__(self, row, rows_used):
        self.rows_used_header = rows_used
        self.number_of_records = None
        self.group_number_of_accounts = None
        self.account_list = []

        if (len(row) > 8):
            print("Too many elements in groupHeader ", row)
            raise BadElementCountException('Group header in bai file has too many elements: expecting 8 got ' + str(len(row)))

        if (len(row) < 8):
            print("Not enough elements in GroupHeader ", row)
            raise BadElementCountException('Group header in bai file has too few elements: expecting 8 got ' + str(len(row)))

        self.record_code = row[0] ##should be 2
        self.ultimate_receiver_id = row[1] #OPTIONAL? final reciever of this group of data
        self.originator_id = row[2]
        self.group_status = row[3] #1 update, 2 deletion, 3 correction, 4 test only
        self.as_of_date = row[4] #YYMMDD originator date
        self.as_of_time = row[5] #optional HHMM 9999 could mean end of date aka 2400
        self.as_of_datetime = DateTimeConverter(self.as_of_date, self.as_of_time)
        self.currency_code = row[6]

        if not row[7].endswith('/'):
            print("group header expects a / at end of line")
        else:
            self.as_of_date_modifier = row[7][:-1] #optional 1 interim previous day data, 2 final previous day 3 interim same day, 4 final same day
                                        # as of date modifier doe snot affect processing, only for reference

    def trailer(self,row, rows_used):
        """group trailer"""
        if (len(row) > 4):
            print("Too many elements in groupTrailer ", row)
            raise BadElementCountException('Group trailer in bai file has too many elements: expecting 4 got ' + str(len(row)))
        if (len(row) < 4):
            print("Not enough elements in GroupTrailer ", row)
            raise BadElementCountException('Group header in bai file has too few elements: expecting 4 got ' + str(len(row)))
        self.rows_used_trailer = rows_used
        self.group_control_total = row[1] #sum of account control totals in this group

        self.group_number_of_accounts  = int(row[2])# should be number of 03 records in this group

        if self.group_number_of_accounts != self.get_account_count():
            print('ERROR the number of accounts %d does not match the expected count %d '%(self.get_account_count(),self.group_number_of_accounts))
            raise BadElementCountException('number of accounts %d does not match expected count %d' % (self.get_account_count(), self.group_number_of_accounts))
        else:
            print('    GOOD NEWS: number of accounts in group matches %d',self.group_number_of_accounts)

        if not row[3].endswith('/'):
            print("groupTrailer expects a / at end of line")
        else:
            self.number_of_records = int(int(row[3][:-1])) #should be number of all records in this group: include 02, 03, 16, 49, and 88 records.  And this 98 record
        if self.number_of_records != self.get_record_count():
            print('ERROR group trailer the number of records %s does not match the expected count %d'%(self.get_record_count(),self.number_of_records))
            raise RecordcountMismatchException('The number of records expected in the group %d  does not match the number found %d'%(self.number_of_records,self.get_record_count()))
        else:
            print('    GOOD NEWS: group trailer number of records matches expectations %d'%self.number_of_records)

    def add_account_header(self,new_account_header):
        """group add account to group"""
        self.account_list.append(new_account_header)

    def get_account_count(self):
        """get account count"""
        return len(self.account_list)

    def get_record_count(self):
        """get record count"""
        total_records = 0
        for a in self.account_list:
            total_records += a.get_record_count()
        return total_records + self.rows_used_header + self.rows_used_trailer

    def get_group_totals(self):
        """group get group totals"""
        total = 0.0
        for a in self.account_list:
            total += a.get_transaction_totals() + a.get_child_totals()
        return total

    def __repr__(self):
        return 'Group header as of ' + self.as_of_date

group_header = None

class AccountHeader:
    def __init__(self, row, rows_used):
        children = (len(row) - 3)/4
        leftovers = (len(row) - 3)%4
        if leftovers > 0:
            print("ERROR wrong number of header colums in AccountHeader ", row)
            raise BadElementCountException('Account header in bai file has wrong number of elements: expecting 3+n*4, got ' + str(len(row)))
        else:
            print("Perfect number of columns in header for "+ str(children)+" children")
        self.rows_used_header = rows_used

        self.transaction_list = []
        self.account_child = []

        self.running_total = 0

        self.record_code = row[0]
        self.customer_account_num = row[1]
        self.currency_code = row[2]
        if not row[len(row)-1].endswith('/'):
            print("accountHeader expects a / at end of line")
        else:
            row[len(row)-1] = row[len(row)-1][:-1]

        for i in range(int(children)):
            #the children can be just little dictionaries.  simple, no need for an object
            AccountHeaderChild = {'type_code':row[3+i*4],
                                  'amount':row[3+i*4+1],
                                  'item_count':row[3+i*4+2],
                                  'funds_type':row[3+i*4+3]}
            self.account_child.append(AccountHeaderChild)



    def trailer(self, row, rows_used):
        """account trailer"""
        self.rows_used_trailer = rows_used
        self.account_control_total = int(row[1])
        if self.account_control_total != self.get_transaction_totals() + self.get_child_totals():
            print('ERROR account trailer total %d does not equal transaction totals %d'%(self.account_control_total,self.get_transaction_totals()))
            raise AmountMismatchException('account trailer total mismatch expecting '+str(self.account_control_total)+ ' but got '+str(self.get_transaction_totals() + self.get_child_totals() ) )
        else:
            print('    GOOD NEWS: account trailer total matches'%self.account_control_total)

        if not row[2].endswith('/'):
            print("account_trailer expects a / at end of line")
        else:
            self.num_of_records = int(row[2][:-1])

        if self.num_of_records != self.get_record_count() : # the 2 accounts for header/trailer
            print('ERROR Account trailer number of records %d does not match expected value %d'%(self.get_record_count(),self.num_of_records))
            raise RecordcountMismatchException('The number of records expected in the account %d  does not match the number found %d'%(self.num_of_records,self.get_record_count()))
        else:
            print('     GOOD NEWS: account trailer number of records matches expectations %d'%self.num_of_records)

    def add_transaction(self,new_transaction):
        """add transaction"""
        self.transaction_list.append(new_transaction)

    def get_record_count(self):
        """account get record count"""
        rows_total = 0;
        for t in self.transaction_list:
            rows_total += t.get_rows_used()
        return rows_total + self.rows_used_header + self.rows_used_trailer

    def get_child_totals(self):
        """account get child totals"""
        total = 0.0
        for ac in self.account_child:
            total += float(ac['amount'] or 0)
        return total

    def get_transaction_totals(self):
        """account get transaction totals"""
        total = 0.0
        for td in self.transaction_list:
            total += td.get_amount()
        return total

current_account_header = None


class TransactionDetail:
    def __init__(self, row, rows_used):
        self.rows_used = rows_used
        self.record_code = row[0]
        self.type_code = row[1]
        self.amount = int(row[2])
        self.funds_type = row[3]
        offset = 0
        if self.funds_type=='S':
            offset = 3
            self.immediate_amount = row[4]
            self.one_day_amount = row[5]
            self.more_than_one_day_amount = row[6]
        else:
            self.immediate_amount = None
            self.one_day_amount = None
            self.more_than_one_day_amount = None
        self.bank_reference_number = row[4 + offset]
        self.customer_reference_number = row[5 + offset]
        self.text = row[6 + offset]
        print('I have a transaction detail with amount %d'%self.amount)

        if (len(row) > 7 + offset):
            print("Too many elements in TransactionDetail ", row)
            raise BadElementCountException('transaction detail in bai file has too many elements: expecting '+len(row) > 7 + offset+' and got ' + str(len(row)))
        if (len(row) < 7 + offset):
            print("Not enough elements in TransactionDetail ", row)
            raise BadElementCountException('transaction detail in bai file has too few elements: expecting '+len(row) > 7 + offset+' and got ' + str(len(row)))


    def get_amount(self):
        """transaction detail get amount"""
        return self.amount

    def get_rows_used(self):
        """transaction detail get rows used"""
        return self.rows_used

def generate_next_line( filename):
    """generate next line.  This generator combines continuation rows with the first row to make one big row to pass up"""
    try:
        with open(filename) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            more_to_read = True
            rows_used = 0
            while more_to_read:
                last_row = []
                for row in csv_reader:
                    if row[0] == '88':
                        #This is a continuation
                        #print('We found a Continuation row', row)
                        del(row[0])
                        if last_row[len(last_row)-1].endswith('/'):
                            last_row[len(last_row)-1] = last_row[len(last_row)-1][:-1]
                        last_row.extend(row)
                        rows_used = rows_used + 1
                        #print('     we extended that row to be ',last_row)
                    else: #This is not a continuation, so we might have a row
                        #print('we just read a row, but not a continuation ', row)
                        if len(last_row) > 0: #we have something we were working on
                            yield(last_row,rows_used)
                            last_row = []
                            rows_used = 0
                        rows_used = rows_used + 1
                        last_row = row
                more_to_read = False
                csv_file.close()
                print("Closing file")
                if len(last_row): #we have a leftover row we have to process
                    yield(last_row, rows_used)
    except FileNotFoundError:
        print("Couldn't open the file ", filename)

    except Exception as e:
        print("Unhandled exception in line generator ", e)

    finally:  #we never actually get here. we just don't get called anymore after the last yield
        csv_file.close()
        print("Closing file")

def read_one_bai_file(filename):

    gen = generate_next_line(filename)

    keepGoing = True
    line_count = 0
    while keepGoing:
        try:
            row,rows_used = next(gen)
            print("generator just made ",row)
            line_count += 1

            row_type = row[0]

            if row_type == '01':
                print("this is a file header row")
                file_header = FileHeader(row, rows_used)

            if row_type == '02':
                print("This is a group header row")
                group_header = GroupHeader(row, rows_used)
                if file_header:
                    file_header.add_group(group_header)
                else:
                    print('ERROR we have a group header, but no file to put it in ',line_count)
                    raise InvalidFileStructureException(' group header not inside file ')

            if row_type == '03':
                ## we should be in the middle of a group to get this sort of row to process
                print("This is an account identifier and summary status row")
                current_account_header = AccountHeader(row, rows_used)
                if group_header:
                    group_header.add_account_header(current_account_header)
                else:
                    print('ERROR we have an account header but not within a group ',line_count)
                    raise InvalidFileStructureException(' Account header not inside group ')

            if row_type == '16':
                print("This is a transaction detail row")
                transaction_detail = TransactionDetail(row, rows_used)
                if current_account_header:
                    current_account_header.add_transaction(transaction_detail)
                else:
                    print('ERROR we have a transaction detail row that is not inside an account', line_count)
                    raise InvalidFileStructureException('Transaction detail not inside account')

            # BTW we will never get a type 88, those get processed in the line generator

            if row_type == '49':
                print("This is an account trailer  row")
                if current_account_header:
                    current_account_header.trailer(row, rows_used)
                    current_account_header = None
                else:
                    print('ERROR we have an account trailer but we are not inside an account ',line_count)
                    raise InvalidFileStructureException(' Account trailer but not inside account. ')

            if row_type == '98':
                print("This is an group trailer  row")
                if group_header:
                    group_header.trailer(row, rows_used)
                    #I think some checking gets done here
                    group_header = None # we are now done with that group
                else:
                    print('ERROR we got a group trailer record, but we have no open group ', line_count)
                    raise InvalidFileStructureException(' Group trailer but not inside group. ')

            if row_type == '99':
                print("This is an file trailer  row")
                if file_header:
                    file_header.file_trailer(row, rows_used)
                else:
                    print('ERROR we have a file trailer but no actual file ',line_count)
                    raise InvalidFileStructureException(' File trailer but not inside file. ')

        except StopIteration:
            keepGoing = False

        except BaiFileException as err:
            print("reRaising baiFileException!")
            raise err

        except Exception as e:
            print("some other error",e)

        print('Processed ',line_count,' lines.')
    return file_header
 #       if file_header:
 #           print(file_header)

