from tkinter import *
from tkinter import filedialog
from tkinter import ttk

from main import readInputsAndWriteOutput

window = Tk()


class rms_gui:
    def __init__(self,window):
        self.window = window


    def define_payerPayment(self):
        self.payerPayment_filename = filedialog.askopenfilename(initialdir="/", title="PayerPayment file",
                                   filetypes=(("all files", "*.*"),("text files", "*.txt")))
        self.payerpayment_label.config(text=self.payerPayment_filename)

    def define_plb(self):
        self.plb_filename = filedialog.askopenfilename(initialdir="/", title="plb file",
                                   filetypes=( ("all files", "*.*"),("text files", "*.txt")))
        self.plb_label.config(text=self.plb_filename)

    def define_bai(self):
        self.bai_filename = filedialog.askopenfilename(initialdir="/", title="bai file",
                                   filetypes=( ("text files", "*.txt"),("all files", "*.*")))
        self.bai_label.config(text=self.bai_filename)

    def define_rms(self):
        self.rms_filename = filedialog.askopenfilename(initialdir="/", title="RMS file",
                                   filetypes=( ("spreadsheet files", "*.xlsx"),("all files", "*.*")))
        self.rms_label.config(text=self.rms_filename)

    def define_output(self):
        self.output_filename = filedialog.asksaveasfilename(initialdir="/", title="output file",
                                   filetypes=( ("spreadsheet files", "*.xlsx"),("all files", "*.*")))
        self.output_label.config(text=self.output_filename)

    def run(self):
        print("RUN THE FILES")
        readInputsAndWriteOutput(
            output_filename=self.output_filename,
            bai_filename=self.bai_filename,
            payerpayment_filename=self.payerPayment_filename,
            plb_filename=self.plb_filename,
            rms_spreadsheet_filename=self.rms_filename,
            rms_spreadsheet_month=int(self.month_combo.get()),
            rms_spreadsheet_day=int(self.day_combo.get())
        )

    def createWindow(self):
        window.title("RMS report")
        window.geometry('500x500')
        self.payerpayment_label = Label(window, text="select file", anchor=W, width=40)
        self.payerpayment_label.grid(column=1, row=1)
        payerpayment_btn = Button(window, text="PayerPayment File", command=self.define_payerPayment)
        payerpayment_btn.grid(column=0, row=1)

        self.plb_label = Label(window, text="select file", anchor=W, width=40)
        self.plb_label.grid(column=1, row=2)
        plb_btn = Button(window, text="plb  File", command=self.define_plb)
        plb_btn.grid(column=0, row=2)

        self.bai_label = Label(window, text="select file", anchor=W, width=40)
        self.bai_label.grid(column=1, row=3)
        bai_btn = Button(window, text="bai File", command=self.define_bai)
        bai_btn.grid(column=0, row=3)

        self.rms_label = Label(window, text="select file", anchor=W, width=40)
        self.rms_label.grid(column=1, row=4)
        rms_btn = Button(window, text="rms File", command=self.define_rms)
        rms_btn.grid(column=0, row=4)

        self.output_label = Label(window, text="name file", anchor=W, width=40)
        self.output_label.grid(column=1, row=5)
        output_btn = Button(window, text="output File", command=self.define_output)
        output_btn.grid(column=0, row=5)

        self.month_combo = ttk.Combobox(window,values=[
                                        "1","2","3","4","5","6",
                                        "7", "8", "9", "10", "11", "12"])
        self.month_combo.grid(column=0, row=6)
        self.month_combo.current(5)

        self.day_combo = ttk.Combobox(window,values=[
                                        "1","2","3","4","5","6","7", "8", "9", "10",
                                        "11","12","13","14","15","16","17", "18", "19", "20",
                                        "21", "22", "23", "24", "25", "26", "27", "28", "29", "30",
                                        "31"])
        self.day_combo.grid(column=1, row=6)
        self.day_combo.current(0)

        go_btn = Button(window, text="Run", command=self.run)
        go_btn.grid(column=0, row=7)

win = rms_gui(window)
win.createWindow()

window.mainloop()
