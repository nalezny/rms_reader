import openpyxl
from openpyxl.styles import Border, Side, PatternFill, Font, GradientFill, Alignment
import csv
import datetime
import read_bai_file

#bai_filename = "C:\\rmsreport\\June17\\6-17Bai.txt"
#payerpayment_filename = 'c:\\rmsreport\\June17\\payerpayment_06182019041240'
#plb_filename = "c:\\rmsreport\\June17\\plbpayment_06182019041240"
#rms_spreadsheet_filename= "C:\\rmsreport\\June11\\Finalytics Deposits Report 06-01.xlsx"
#rms_spreadsheet_month = 6
#rms_spreadsheet_day = 17
#output_filename = "C:\\rmsreport\\summaryJune17.xlsx"

claim_list = []
matchedClaims = dict()


def parse_lockbox(filename ):
    """Balance payerpayment"""

    #claim_list = []
    with open(filename, newline='', encoding='utf-8-sig') as csvfile:
        csvreader = csv.reader(csvfile, delimiter=',')
        for row in csvreader:
            if row[0] == 'H':
                claim_list.append([row[1], row[3]])

def find_expected_payment(id):
    """find expected payment"""
    for i,p in claim_list:
        if i == id:
            matchedClaims[id] = p
            if isinstance(p,str):
                return p
            else:
                return '%6.2f' % p
    return "xxxNOTFOUNDxxx"


def read_plbfile(filename):
   # filename = "c:\\rmsreport\\June17\\plbpayment_06182019041240"
    plb_list = []
    with open(filename, newline='', encoding='utf-8-sig') as csvfile:
        csvreader = csv.reader(csvfile, delimiter=',')
        for row in csvreader:
            plb_list.append((row[0], row[1],row[2]))

    return plb_list


def read_rms_spreadsheet(filename,rms_spreadsheet_month, rms_spreadsheet_day):
    print("In read_rms_spreadsheet")
    wb_obj = openpyxl.load_workbook(filename)

    sheet_obj = wb_obj.active

    cell_obj = sheet_obj.cell(row=1, column=1)

    dates = []
    for r in range(2, sheet_obj.max_row + 1):
        d_col = sheet_obj.cell(row=r, column=6)
        #HARDCODED date:  we are only looking at this date for now...
#        if not d_col.value in dates and d_col.value == datetime.datetime(2019, 5, 13, 0, 0):
        if not d_col.value in dates and d_col.value == datetime.datetime(2019, rms_spreadsheet_month, rms_spreadsheet_day, 0, 0):
                dates.append(d_col.value)

    print("Here are the dates I found:")
    print(dates)

    info_on_all_dates = []
    for d in dates:
        date_info = {}
        date_info['date']=d

        #within this date, lets find all the lockbox entries
        # this will be inefficient, in that it reads the whole spreadsheet every time.  But I'm not sure I really care...
        top = 100000
        bottom = 0
        for r in range(2,sheet_obj.max_row+1):
            d_col = sheet_obj.cell(row=r, column=6+1)
            if d_col.value == d:
                if r < top:
                    top = r
                if r > bottom:
                    bottom = r
        print("between rows " + str(top) + " and " + str(bottom))
        #ok now go get the list of batch values
        batches = []
        for r in range(top,bottom+1):
            batch_name = sheet_obj.cell(row=r,column=2+1)
            amount = sheet_obj.cell(row=r, column=11+1)
            if amount.value != 0 and amount.value != 'NULL':
                if batch_name.value != 'NONE' and not batch_name.value in batches:
                    batches.append(batch_name.value)

        print("batches " + str(batches))

        lockbox_transactions=[]
        for batch in batches:
            total = 0
            for r in range(top, bottom + 1):
                b = sheet_obj.cell(row=r, column=2+1)
                amount = sheet_obj.cell(row=r, column=11+1)
                if b.value == batch and amount.value != 'NULL':
                    total = total + float(amount.value)
                print("   batch " + str(batch) + " total:" + str(total) )

            lockbox_transactions.append((batch, total))


        #date_info['lockbox_transactions',lockbox_transactions]

        #now get the list of checks
        checks = []
        for r in range(top, bottom+1):
            batch_name = sheet_obj.cell(row=r, column=2+1)
            check_num = sheet_obj.cell(row=r,column=3+1)
            amount = sheet_obj.cell(row=r,column=11+1).value

            if batch_name.value == 'NONE' and amount != 'NULL' and float(amount) > 0:
                if not check_num.value in checks:
                    checks.append(check_num.value)

        #now I have a list of all check numbers.  I have to go back and get a list of items for each check
        check_details = []
        for c in checks:
            for r in range(top,bottom+1):
                check_num = sheet_obj.cell(row=r, column=3+1)
                if c == check_num.value:
                    check_amount = sheet_obj.cell(row=r, column=4+1).value
                    paid_amount = sheet_obj.cell(row=r, column=11+1).value
                    tx_id = sheet_obj.cell(row=r, column=10+1).value
                    plb_id = sheet_obj.cell(row=r,column=2).value
                    check_details.append((c,check_amount,paid_amount,tx_id,plb_id))

        #date_info.append( d,lockbox_transactions,checks)
        info_on_all_dates.append( (d,lockbox_transactions,checks,check_details))
    print("exiting Read_rms_spreadsheet")
    return info_on_all_dates

def readInputsAndWriteOutput(output_filename,bai_filename,payerpayment_filename,plb_filename,rms_spreadsheet_filename,rms_spreadsheet_month, rms_spreadsheet_day):
    color_array = ["FF00FF", "00FFFF", "FFFF00", "FF0000", "008000", '800080', '808080', 'FF8040', 'C0C0C0']

    match_color = 0

    parse_lockbox(payerpayment_filename)
    info_on_all_dates = read_rms_spreadsheet(rms_spreadsheet_filename,rms_spreadsheet_month, rms_spreadsheet_day )

    output_wb = openpyxl.Workbook()
    output_sheet = output_wb.active
    output_column = 1
    output_row = 1

    parse_lockbox(payerpayment_filename)

    print("PLB file")
    plb_records = read_plbfile(plb_filename)
    for (id,payer,amount) in plb_records:
        print(id,"<>",payer,"<>",amount)


    print("Here are the dates I found:")
    for d,transactions,checks,check_details in info_on_all_dates:
        print('dates<> ' + str(d) + ' has ' + str(len(transactions)) + ' transaction and '+ str(len(checks)) + ' checks:details '+str(check_details))
        output_cell = output_sheet.cell(row=output_row, column=output_column)
        output_cell.value = str(d)
        output_cell.style='Title'

        output_row = 3
        output_cell = output_sheet.cell(row=output_row, column=output_column)
        output_cell.value = 'Lockbox transactions'
        output_row = output_row + 1
        lockbox_transactions = []

        output_cell = output_sheet.cell(row=output_row, column=2)
        for t in transactions:
            (transaction_id, transaction_total) = t
            output_cell = output_sheet.cell(row=output_row, column=output_column)
            output_cell.value = "id:" + str(transaction_id) + " total: %6.2f" % transaction_total
            lockbox_transactions.append((transaction_id,transaction_total,output_cell))
            output_row += 1


        output_row += 1
        output_cell = output_sheet.cell(row=output_row, column=1)
        output_row = output_row + 1
        output_cell.value="Checks"
        output_row = output_row + 1

        check_values = []
        for c in checks:
            output_cell = output_sheet.cell(row=output_row, column=output_column )
            output_cell.value = c
            output_cell.number_format = '0'
            output_row += 1
            paid_total = 0
            check_amount = 0

            for cd in check_details:
                (cn, c_a, paid_amount, tx_id,plb_id) = cd
                if c == cn:
                    if paid_amount != 'NULL':
                        paid_total = paid_total + float(paid_amount)
                        check_amount = c_a
                        if paid_amount > 0:
                            output_cell = output_sheet.cell(row=output_row, column=output_column + 1)
                            output_row = output_row + 1
                            output_cell.value = "%6.2f id " % paid_amount + tx_id + " expected: " + find_expected_payment(tx_id) + " plb_id:" + str(plb_id)


            output_cell = output_sheet.cell(row=output_row, column=output_column)
            output_cell.value = "Sum of amounts"
            output_cell2 = output_sheet.cell(row=output_row, column=output_column+1)
            output_cell2.value = "%6.2f:"% paid_total
            output_cell3 = output_sheet.cell(row=output_row, column=output_column+2)
            output_cell3.value = "check total"
            output_cell4 = output_sheet.cell(row=output_row, column=output_column+3)
            output_cell4.value = "%6.2f"% check_amount
            output_cell5 = output_sheet.cell(row=output_row, column=output_column + 4)
            output_cell5.value = "difference"
            output_cell6 = output_sheet.cell(row=output_row, column=output_column + 5)
            output_cell6.value = "%6.2f"%(paid_total - check_amount)

            check_values.append((c,check_amount,paid_total,output_cell,output_cell6,plb_id))
            output_row = output_row + 2

        used = 0
        notused = 0
        output_cell = output_sheet.cell(row=output_row, column=output_column)
        output_row = output_row + 1
        output_cell.value = "Unused payerpayment information"

        for i, p in claim_list:
            if i in matchedClaims:
                used = used + 1
            else:
                notused = notused + 1
                output_cell = output_sheet.cell(row=output_row, column=output_column)
                output_cell.value = i
                output_cell = output_sheet.cell(row=output_row, column=output_column+1)
                output_cell.value = '<>'
                output_cell = output_sheet.cell(row=output_row, column=output_column+2)
                output_cell.value = p
                output_row = output_row + 1
        print("used ", used)
        print("not used", notused)

        file_header = read_bai_file.read_one_bai_file( bai_filename )

        output_row = output_row + 1
        output_cell = output_sheet.cell(row=output_row, column=output_column)
        output_row = output_row + 1
        output_cell.value = "transactions from bai file"


    for g in file_header.group_list:
        print(g.as_of_datetime)
        for a in g.account_list:
            for td in a.transaction_list:
                print("we have a transaction of ", td.amount/100, "  ", td.text)
                output_cell1 = output_sheet.cell(row=output_row, column=output_column)
                output_cell1.value = str(td.amount/100)
                output_cell = output_sheet.cell(row=output_row, column=output_column+1)
                output_cell.value = td.text
                output_row = output_row + 1
                #do we have a check of this amount?  maybe we can check it off our list
                for t,ca,ct,cell,cell_diff,plb_id in check_values:
                    if int(round(ca*100)) == td.amount or int(round(ct*100))==td.amount:
                        cell.fill = PatternFill("solid", fgColor=color_array[match_color])
                        output_cell1.fill = PatternFill("solid", fgColor=color_array[match_color])
                        match_color += 1
                        match_color = match_color % len(color_array)
                #would you believe a lockbox transaction?
                for t,amount,cell in lockbox_transactions:
                    if int(round(amount*100)) == td.amount:
                        cell.fill = PatternFill("solid", fgColor=color_array[match_color])
                        output_cell1.fill = PatternFill("solid", fgColor=color_array[match_color])
                        match_color += 1
                        match_color = match_color % len(color_array)

    output_row = output_row + 1
    output_cell = output_sheet.cell(row=output_row, column=output_column)
    output_cell.value = "PLB records"
    output_row = output_row + 1
    for (id,payer,amount) in plb_records:
        output_cell1 = output_sheet.cell(row=output_row, column=output_column)
        output_cell1.value = id
        output_cell2 = output_sheet.cell(row=output_row, column=output_column+1)
        output_cell2.value = amount
        output_row = output_row + 1
        # do we have a check of this amount?  maybe we can check it off our list
        for t, ca, ct, cell, cell_diff,plb_id in check_values:
            print("plb comparing " + str(amount) + " to " +str(ct-ca))
            if abs( (ct-ca) - float(amount)) < 0.0001:
                print("match")
                cell_diff.fill = PatternFill("solid", fgColor=color_array[match_color])
                output_cell1.fill = PatternFill("solid", fgColor=color_array[match_color])
                match_color += 1
                match_color = match_color % len(color_array)

    output_wb.save(output_filename)


#readInputsAndWriteOutput(output_filename = "C:\\rmsreport\\summaryJune17y.xlsx",
##                            bai_filename = "C:\\rmsreport\\June17\\6-17Bai.txt",
#                            payerpayment_filename='c:\\rmsreport\\June17\\payerpayment_06182019041240',
##                            plb_filename = "c:\\rmsreport\\June17\\plbpayment_06182019041240",
#                            rms_spreadsheet_filename = "C:\\rmsreport\\June11\\Finalytics Deposits Report 06-01.xlsx",
#                            rms_spreadsheet_month=6,
#                            rms_spreadsheet_day = 17
#)